package services;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import entities.Basket;
import entities.Item;
import entities.Offer;
import entities.Product;

public class DiscountServiceTest {
	
	DiscountService discountService= new DiscountService();
	
	

	@Test
	public void testCountDiscount() {
		Product p1 = new Product(Item.Apple,new BigDecimal(0.20));
		Offer o1=new Offer(p1,new BigDecimal(3),new BigDecimal(0));
		Offer offerResult= new Offer();
		offerResult=this.discountService.countDiscount(o1);
		assertTrue(offerResult.getItemTotalPrice().floatValue()==new BigDecimal(0.60).floatValue());
		assertTrue(offerResult.getQuantity().intValue()==6);
		Product p2 = new Product(Item.Orange,new BigDecimal(0.50));
		Offer o2=new Offer(p2,new BigDecimal(9),new BigDecimal(0));
		offerResult=this.discountService.countDiscount(o2);
		assertTrue(offerResult.getItemTotalPrice().floatValue()==4.5);
		assertTrue(offerResult.getQuantity().intValue()==9);
		Product p3 = new Product(Item.Watermelon,new BigDecimal(0.80));
		Offer o3=new Offer(p3,new BigDecimal(9),new BigDecimal(0));
		offerResult=this.discountService.countDiscount(o3);
		System.out.println(offerResult.getItemTotalPrice().floatValue());
		assertTrue(offerResult.getItemTotalPrice().floatValue()==new BigDecimal(4.80).floatValue());
		assertTrue(offerResult.getQuantity().intValue()==9);
	}

	@Test
	public void testCountFinalAmount() {
		Product p1 = new Product(Item.Apple,new BigDecimal(0.20));
		Product p2 = new Product(Item.Orange,new BigDecimal(0.50));
		Product p3 = new Product(Item.Watermelon,new BigDecimal(0.80));
		Offer o1=new Offer(p1,new BigDecimal(3),new BigDecimal(0));
		Offer o2=new Offer(p2,new BigDecimal(9),new BigDecimal(0));
		Offer o3=new Offer(p3,new BigDecimal(9),new BigDecimal(0));
		List<Offer> offers = new ArrayList<Offer>();
		offers.add(o1);
		offers.add(o2);
		offers.add(o3);
		Basket basket = new Basket();
		basket.setTotalPrice(new BigDecimal(0));
		basket.setProducts(offers);
		DiscountService ds = new DiscountService();
		Basket basketFinal =ds.countFinalAmount(basket);
		assertTrue(basketFinal.getTotalPrice().floatValue()==new BigDecimal(9.90).floatValue());
	}

}
