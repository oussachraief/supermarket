package main;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import entities.Basket;
import entities.Item;
import entities.Offer;
import entities.Product;
import services.DiscountService;

public class MainClass {
	
	

	public static void main(String[] args) {
		Product p1 = new Product(Item.Apple,new BigDecimal(0.20));
		Product p2 = new Product(Item.Orange,new BigDecimal(0.50));
		Product p3 = new Product(Item.Watermelon,new BigDecimal(0.80));
		Offer o1=new Offer(p1,new BigDecimal(3),new BigDecimal(0));
		Offer o2=new Offer(p2,new BigDecimal(9),new BigDecimal(0));
		Offer o3=new Offer(p3,new BigDecimal(9),new BigDecimal(0));
		List<Offer> offers = new ArrayList<Offer>();
		offers.add(o1);
		offers.add(o2);
		offers.add(o3);
		Basket basket = new Basket();
		basket.setTotalPrice(new BigDecimal(0));
		basket.setProducts(offers);
		DiscountService ds = new DiscountService();
		Basket basketFinal =ds.countFinalAmount(basket);
		System.out.println(basketFinal.getTotalPrice());
		
	}
		
		
		
		
}
