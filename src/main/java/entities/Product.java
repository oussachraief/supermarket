package entities;

import java.math.BigDecimal;

/**
 * @author CH.O
 *  Represents a product with type and uniter price
 */
public class Product {
	/** The type of the Item */
	private Item item;
	/** the price per unity */
	private BigDecimal price;
	
	
	
	public Product() {
		
	}
	
	
	
	public Product(Item item, BigDecimal price) {
		
		this.item = item;
		this.price = price;
	}



	
	public Item getItem() {
		return item;
	}



	public void setItem(Item item) {
		this.item = item;
	}



	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [item=" + item + ", price=" + price + "]";
	}
	
	
	
}
