package entities;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author CH.O
 * Represents a basket for all products with final amount to pay
 */
public class Basket {
	/** List of all the products after calculating their prices for the given quantity and applicating discount*/
	private List<Offer> products; 
	/** final amount to pay */
	private BigDecimal totalPrice;
	
	
	public Basket() {
		
	}
	public List<Offer> getProducts() {
		return products;
	}
	public void setProducts(List<Offer> products) {
		this.products = products;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	
}
