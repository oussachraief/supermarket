package entities;

import java.math.BigDecimal;

/**
 * @author CH.O
 * Represents a basket for a single product  
 */
public class Offer {
	/** the product */
	private Product product;
	/** wanted quantity of the product */
	private BigDecimal quantity; 
	/** total amount of the product for the given quantity after application of discount */
	private BigDecimal itemTotalPrice;
	
	public Offer() {
		
	}
	
	

	public Offer(Product product, BigDecimal quantity, BigDecimal itemTotalPrice) {
		
		this.product = product;
		this.quantity = quantity;
		this.itemTotalPrice = itemTotalPrice;
	}



	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	
	
	

	public BigDecimal getQuantity() {
		return quantity;
	}



	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}



	public BigDecimal getItemTotalPrice() {
		
		return itemTotalPrice;
	}

	public void setItemTotalPrice(BigDecimal itemTotalPrice) {
		this.itemTotalPrice = itemTotalPrice;
	}
	
	
	
	
	
	
}
