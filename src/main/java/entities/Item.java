package entities;


/**
 * @author CH.O
 * Enumerates the Items available in the market   
 */
public enum Item {
	Apple,
	Orange,
	Watermelon
}
