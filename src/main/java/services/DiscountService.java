package services;

import java.math.BigDecimal;

import entities.Basket;
import entities.Item;
import entities.Offer;

/**
 * @author CH.O
 *
 */
public class DiscountService {
	
	/**
    	Calculate total price of a product after applicating discountes
    	@param offer represents the quantity , price and type of Item , to be calculated 
    	@return Offer after being modified containing the total amount , and quantity of the Item after discount.
	 */
	public Offer countDiscount(Offer offer) {
		//if Apple applicate discount relative to this Item
		if(offer.getProduct().getItem().equals(Item.Apple)) {
			offer.setItemTotalPrice(offer.getProduct().getPrice().multiply(offer.getQuantity()));
			offer.setQuantity(offer.getQuantity().multiply(new BigDecimal(2)));
			return offer;
		}else {
			//if Watermelon applicate discount relative to this Item
			if(offer.getProduct().getItem().equals(Item.Watermelon)) {
				offer.setItemTotalPrice(offer.getProduct().getPrice().multiply(offer.getQuantity().subtract(offer.getQuantity().divide(new BigDecimal(3)))));
				return offer;
			}else {
				// Item without discount
				offer.setItemTotalPrice(offer.getProduct().getPrice().multiply(offer.getQuantity()));
			}
		}
		
		
		return offer;
		
	}
	
	/**
		Calculate total price of all products already discounted 
		@param basket containing offers after discount and non calculated total amount of the basket
		@return Basket containing offers after discount and calculated total amount of the basket
	 */
	public Basket countFinalAmount(Basket basket) {
		
		for (Offer offer : basket.getProducts()) {
			
			basket.setTotalPrice(basket.getTotalPrice().add(countDiscount(offer).getItemTotalPrice()));
		}
		return basket;
	}
	
}
